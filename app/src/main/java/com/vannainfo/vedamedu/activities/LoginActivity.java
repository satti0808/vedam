package com.vannainfo.vedamedu.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.vannainfo.vedamedu.R;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    EditText editUserName, editPassword;
    Button btnLogin, btnSignUp;

    TextView textForgotPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editUserName = (EditText) findViewById(R.id.et_username);
        editPassword = (EditText) findViewById(R.id.et_password);

        btnLogin = (Button) findViewById(R.id.btn_login);
        btnSignUp = (Button) findViewById(R.id.btn_register);
        textForgotPass = (TextView) findViewById(R.id.tv_forgot_password);

        btnLogin.setOnClickListener(this);
        btnSignUp.setOnClickListener(this);
        textForgotPass.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v == btnLogin){

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);

            finish();
        }else if(v == btnSignUp){

        }else if(v == textForgotPass){

        }
    }
}