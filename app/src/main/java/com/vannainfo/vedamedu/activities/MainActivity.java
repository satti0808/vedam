package com.vannainfo.vedamedu.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.vannainfo.vedamedu.R;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}