package com.vannainfo.vedamedu.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.vannainfo.vedamedu.R;
import com.vannainfo.vedamedu.backendApi.BackendAPIs;
import com.vannainfo.vedamedu.backendApi.RetrofitInterface;
import com.vannainfo.vedamedu.dialogs.LoadingDialog;
import com.vannainfo.vedamedu.localstorage.LocalStorageData;

import org.json.JSONObject;

import java.lang.reflect.Field;

public class BaseActivity extends AppCompatActivity {

    public Toolbar toolbar;
    public ActionBar actionBar;
    public Context context;

    public RetrofitInterface retrofitInterface;
    public JSONObject paramObject;

    Handler myHandler;
    String TAG;

    private LocalStorageData localStorageData;
    private LoadingDialog loadingDialog;
    private BackendAPIs backendAPIs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        actionBar = getSupportActionBar();
        context = this;

        try {
            myHandler = new Handler();
//            retrofitInterface = getBackendAPIs().baseAdapter().create(RetrofitInterface.class);
            paramObject = new JSONObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //==============  ACTIONBAR METHODS  ==============//

    protected void setupActionBar() {
        try {
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            if (toolbar != null) {
                setSupportActionBar(toolbar);
                /*actionbar.setHomeAsUpIndicator(R.drawable.ic_menu);*/
                actionBar = getSupportActionBar();
                if (actionBar != null) {
                    actionBar.setDisplayHomeAsUpEnabled(true);
                    actionBar.setDisplayShowHomeEnabled(true);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setToolbarTitle(String title) {
        try {
            if (toolbar != null) {
                actionBar = getSupportActionBar();
                TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.toolbar_text);
                if (actionBar != null) {
                    actionBar.setTitle("");
                    toolbarTitle.setText(title);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
     *   Initialize LocalStorage Class
     * */
    public LocalStorageData getLocalStorageData() {
        if (localStorageData == null) {
            localStorageData = new LocalStorageData(context);
        }
        return localStorageData;
    }

    /*
     *   Initialize Backened Class
     * */
    public BackendAPIs getBackendAPIs() {
        if (backendAPIs == null) {
            backendAPIs = new BackendAPIs(context);
        }
        return backendAPIs;
    }

    public LoadingDialog getLoadingDialog() {
        closeLoadingDialog();
        if (loadingDialog == null) {
            loadingDialog = new LoadingDialog(this);

        }
        return loadingDialog;
    }

    public void closeLoadingDialog() {
        if (loadingDialog != null) {
            loadingDialog.closeDialog();
        }
    }

    public void showToast(String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public void hideSoftKeyboard() {
        try {
            View view = this.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public final static boolean isValidMobileNumber(CharSequence target) {
        if (target == null || target.length()!=10) {
            return false;
        } else {
            return Patterns.PHONE.matcher(target).matches();
        }
    }
}
